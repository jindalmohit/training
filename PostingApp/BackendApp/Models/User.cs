using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendApp.Models
{
    public class User
    {
        public User() { }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "User name is required")]
        [RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Please enter valid user name.")]
        [StringLength(255)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email Id is required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$", ErrorMessage = "Please enter valid email Id")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [RegularExpression(@"^\d{10}$", ErrorMessage = "Please enter valid phone number")]
        public long Phone { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Date of birth is required")]
        [DataType(DataType.Date)]
        [Display(Name = "Date of Birth")]
        public DateTime BirthDate { get; set; }

        public byte[] ProfileImage { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [RegularExpression(@"^(?=.*\d).{4,8}$", ErrorMessage = "Please enter valid password")]
        public string Password { get; set; }

        public ICollection<Post> Posts { get; set; }

    }
}