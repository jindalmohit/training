using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendApp.Models
{
    public class Post
    {
        public Post() { }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        public string Likes { get; set;}

        // public int CommentCount { get; set; }
        
       /*  [NotMapped]
        public int[] LikeData
        {
            get
            {
                return Array.ConvertAll(Likes.Split(';'), int.Parse);
            }
            set
            {
                Likes = string.Join(",", value);
            }
        } */

        public int UserId { get; set; }

        public User User { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}