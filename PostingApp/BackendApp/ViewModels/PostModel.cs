using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BackendApp.Models;

namespace BackendApp.ViewModels
{
    public class PostModel
    {
        public PostModel() { }

        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        public string Likes { get; set; }

        public int[] LikeData { get; set; }

        public int CommentCount { get; set; }

        // public int[] LikeData
        // {
        //     get
        //     {
        //         if (Likes != null)
        //         {
        //             return Array.ConvertAll(Likes.Split(','), int.Parse);
        //             //return new string[]{"Sweta"};
        //         }
        //         else
        //         {
        //             return null;
        //         }
        //     }
        //     set
        //     {
        //         if (LikeData != null)
        //         {
        //             Likes = string.Join(",", value);
        //         }
        //         else
        //         {
        //             Likes = null;
        //         }
        //     }
        // }

        public int UserId { get; set; }

        public User User { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}