using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BackendApp.Models;

namespace BackendApp.ViewModels
{
    public class CommentModel
    {
        public CommentModel() { }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; }

        [Required]
        public int UserId { get; set; }
        
        public int PostId { get; set; }
        
        public Post Post { get; set; }
    }
}