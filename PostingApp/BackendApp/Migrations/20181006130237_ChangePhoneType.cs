﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendApp.Migrations
{
    public partial class ChangePhoneType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "Phone",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Phone",
                table: "Users",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
