﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BackendApp.Migrations
{
    public partial class RemoveCommentCountFromPostTableAgain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommentCount",
                table: "Posts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CommentCount",
                table: "Posts",
                nullable: false,
                defaultValue: 0);
        }
    }
}
