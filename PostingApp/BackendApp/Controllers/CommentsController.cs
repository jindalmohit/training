using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackendApp.BackendDataContext;
using BackendApp.Models;
using BackendApp.ViewModels;

namespace BackendApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly BackendContext _context;

        public CommentsController(BackendContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IList<CommentModel> GetAll()
        {
            IList<Comment> commentsFromDb = _context.Comments.ToList();
            IList<CommentModel> comments = commentsFromDb.ToList().ConvertAll<CommentModel>(x => new CommentModel
            {
                Id = x.Id,
                Content = x.Content,
                CreatedAt = x.CreatedAt,
                UserId = x.UserId,
                PostId = x.PostId,
                Post = x.Post
            });
            return comments;
        }

        [HttpGet("{id}")]
        public ActionResult<CommentModel> GetById(int id)
        {
            Comment commentFromDb = _context.Comments.Find(id);
            if (commentFromDb == null)
            {
                return NotFound();
            }
            CommentModel comment = new CommentModel()
            {
                Id = commentFromDb.Id,
                Content = commentFromDb.Content,
                CreatedAt = commentFromDb.CreatedAt,
                UserId = commentFromDb.UserId,
                PostId = commentFromDb.PostId,
                Post = commentFromDb.Post
            };
            return comment;
        }

        [HttpGet("postedComments/{postId}")]
        public IList<CommentModel> GetByPostId(int postId)
        {
            IList<Comment> commentsFromDb = _context.Comments.ToList().FindAll(x => x.PostId == postId);
            IList<CommentModel> comments = commentsFromDb.ToList().ConvertAll<CommentModel>(x => new CommentModel
            {
                Id = x.Id,
                Content = x.Content,
                CreatedAt = x.CreatedAt,
                UserId = x.UserId,
                PostId = x.PostId,
                Post = x.Post
            });
            return comments;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Comment> Create(CommentModel commentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            Comment comment = new Comment()
            {
                Content = commentModel.Content,
                CreatedAt = commentModel.CreatedAt,
                UserId = commentModel.UserId,
                PostId = commentModel.PostId
            };
            _context.Comments.Add(comment);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = comment.Id }, comment);
        }

        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Comment> Edit(CommentModel commentModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            Comment comment = _context.Comments.Find(commentModel.Id);
            comment.Content = commentModel.Content;
            comment.CreatedAt = commentModel.CreatedAt;
            comment.UserId = commentModel.UserId;
            comment.PostId = commentModel.PostId;

            _context.Comments.Update(comment);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = comment.Id }, comment);
        }

        [HttpDelete("{id}")]
        public ActionResult<Comment> Delete(int id)
        {
            Comment comment = _context.Comments.Find(id);
            if (comment == null)
            {
                return NotFound();
            }
            _context.Comments.Remove(comment);
            _context.SaveChanges();
            return Ok(comment);
        }
    }
}