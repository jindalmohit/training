using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackendApp.BackendDataContext;
using BackendApp.Models;
using BackendApp.ViewModels;

namespace BackendApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly BackendContext _context;

        public PostsController(BackendContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IList<PostModel> GetAll()
        {
            IList<Post> postsFromDb = _context.Posts.ToList();
            IList<PostModel> posts = postsFromDb.ToList().ConvertAll<PostModel>(x => new PostModel
            {
                Id = x.Id,
                Content = x.Content,
                CreatedAt = x.CreatedAt,
                LikeData = !string.IsNullOrEmpty(x.Likes) ? Array.ConvertAll(x.Likes.Split(','), int.Parse) : null,
                UserId = x.UserId,
                Comments = x.Comments,
                CommentCount = _context.Comments.Where(y => y.PostId == x.Id).Count()
            });
            return posts;
        }

        [HttpGet("{id}")]
        public ActionResult<PostModel> GetById(int id)
        {
            Post postFromDb = _context.Posts.Find(id);
            if (postFromDb == null)
            {
                return NotFound();
            }
            PostModel post = new PostModel()
            {
                Id = postFromDb.Id,
                Content = postFromDb.Content,
                CreatedAt = postFromDb.CreatedAt,
                LikeData = !string.IsNullOrEmpty(postFromDb.Likes) ? Array.ConvertAll(postFromDb.Likes.Split(','), int.Parse) : null,
                UserId = postFromDb.UserId,
                Comments = postFromDb.Comments,
                CommentCount = _context.Comments.Where(y => y.PostId == postFromDb.Id).Count()
            };
            return post;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Post> Create(PostModel postModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Post post = new Post();
            post.Content = postModel.Content;
            post.CreatedAt = postModel.CreatedAt;
            post.Likes = postModel.LikeData != null ? string.Join(",", postModel.LikeData) : string.Empty;
            post.UserId = postModel.UserId;

            _context.Posts.Add(post);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = post.Id }, post);
        }

        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Post> Edit(PostModel postModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Post post = _context.Posts.Find(postModel.Id);
            post.Content = postModel.Content;
            post.CreatedAt = postModel.CreatedAt;
            post.Likes = postModel.LikeData != null ? string.Join(",", postModel.LikeData) : string.Empty;
            post.UserId = postModel.UserId;
            // post.CommentCount = postModel.CommentCount;

            _context.Posts.Update(post);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = post.Id }, post);
        }

        [HttpDelete("{id}")]
        public ActionResult<Post> Delete(int id)
        {
            Post post = _context.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }
            _context.Posts.Remove(post);
            _context.SaveChanges();
            return Ok(post);
        }
    }
}