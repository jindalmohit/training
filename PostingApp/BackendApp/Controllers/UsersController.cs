using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BackendApp.BackendDataContext;
using BackendApp.Models;
using BackendApp.ViewModels;

namespace BackendApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly BackendContext _context;

        public UsersController(BackendContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IList<User> GetAll()
        {
            IList<User> users = _context.Users.ToList();
            return users;
        }

        [HttpGet("{id}")]
        public ActionResult<User> GetById(int id)
        {
            User user = _context.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<User> Create(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = new User();
            user.Name = userModel.Name;
            user.Email = userModel.Email;
            user.Phone = userModel.Phone;
            user.Gender = userModel.Gender;
            user.BirthDate = userModel.BirthDate;
            user.Password = userModel.Password;

            _context.Users.Add(user);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = user.Id }, user);
        }

        [HttpPut]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<User> Edit(UserModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User user = _context.Users.Find(userModel.Id);
            user.Name = userModel.Name;
            user.Email = userModel.Email;
            user.Phone = userModel.Phone;
            user.Gender = userModel.Gender;
            user.BirthDate = userModel.BirthDate;
            user.Password = userModel.Password;

            _context.Users.Update(user);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { Id = user.Id }, user);

        }

        [HttpDelete("{id}")]
        public ActionResult<User> Delete(int id)
        {
            User user = _context.Users.Find(id);
            if (!ModelState.IsValid)
            {
                return NotFound();
            }
            _context.Users.Remove(user);
            _context.SaveChanges();
            return Ok(user);
        }
    }
}