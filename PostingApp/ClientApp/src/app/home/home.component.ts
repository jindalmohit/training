import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { User } from '../Models/User';
import { Post } from '../Models/Post';
import { Router } from '@angular/router';
import { AuthService } from '../Services/auth.service';
import { PostService } from '../Services/post.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../Services/user.service';
import { SharedDataService } from '../Services/shared-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public loggedInUser: User;
  public quillData: any;
  public newPost = new Post('', null, null, null);
  // public allPosts: Post[];
  // public allPostsWithUserDetails: Post[];
  // public allPostsWithCommentCount: Post[];

  // @ViewChild ('myData') myData: ElementRef;

  constructor(
    private router: Router,
    private authService: AuthService,
    private postService: PostService,
    private toastrService: ToastrService,
    private userService: UserService,
    private sharedDataService: SharedDataService
  ) { }

  ngOnInit() {
    this.loggedInUser = localStorage.getItem('loggedInUser') == null ? null : JSON.parse(localStorage.getItem('loggedInUser'));
    // this.postService.getAllPost().subscribe(
    //   res => {
    //     console.log(res);
    //     this.sharedDataService.allPostsAsObservable.next(res);
    //     // this.allPosts = res;
    //     this.sharedDataService.allPostsAsObservable.subscribe(
    //       result => this.allPosts = result
    //     );
    //     console.log(this.allPosts);
    //     // this.getPostsWithComments(this.allPosts);
    //     this.getPostsWithUserDetails(this.allPosts);
    //   },
    //   msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
    // );
  }

  // method(posts) {
  //   /*  console.log(posts);
  //    this.allPosts = posts;
  //    console.log(this.allPosts);
  //    this.getPostsWithUserDetails(this.allPosts); */
  //   this.postService.getAllPost().subscribe(
  //     res => {
  //       console.log(res);
  //       this.allPosts = res;
  //       console.log(this.allPosts);
  //       this.getPostsWithUserDetails(this.allPosts);
  //     },
  //     msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
  //   );
  // }

  // getPostsWithUserDetails(posts) {
  //   this.allPostsWithUserDetails = Object.assign([], posts);
  //   this.allPostsWithUserDetails.forEach((element, index, arr) => {
  //     this.userService.getUserById(element.userId).subscribe(
  //       res => {
  //         element.user = res;
  //       }
  //     );
  //   });
  //   this.setIsPostLikedByLoggedInUser();
  //   console.log(this.allPostsWithUserDetails);
  // }

  // setIsPostLikedByLoggedInUser() {
  //   this.allPostsWithUserDetails.forEach((element, index, arr) => {
  //     if (element.likeData != null) {
  //       if (element.likeData.includes(this.loggedInUser.id)) {
  //         element.isPostLikedByLoggedInUser = true;
  //       } else {
  //         element.isPostLikedByLoggedInUser = false;
  //       }
  //     }
  //   });
  // }

  logout(): void {
    localStorage.removeItem('loggedInUser');
    this.authService.setLoggedInStatus(false);
    this.router.navigate(['/signin']);
  }

  makePost(): void {
    this.newPost.userId = this.loggedInUser.id;
    this.newPost.createdAt = new Date();
    console.log(this.newPost);
    this.postService.addPost(this.newPost).subscribe(
      res => {
        console.log(res);
        this.newPost.content = '';
        this.postService.getAllPost().subscribe(
          result => {
            console.log(result);
            this.sharedDataService.allPostsAsObservable.next(result);
            this.toastrService.success('Posted Successfully', '', {
              positionClass: 'toast-bottom-right'
            });
            // this.allPosts = result;
            // console.log(this.allPosts);
            // this.getPostsWithUserDetails(this.allPosts);
          },
          msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
        );
      }
    );
  }

}
