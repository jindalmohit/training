import { HomeComponent } from '../home/home.component';
import { SigninComponent } from '../signin/signin.component';
import { PostService } from '../Services/post.service';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AuthService } from '../Services/auth.service';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { User } from '../Models/User';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { ListPostsComponent } from '../list-posts/list-posts.component';
import { AvatarModule } from 'ngx-avatar';
import { AddOrListCommentsComponent } from '../add-or-list-comments/add-or-list-comments.component';
import { SharedDataService } from '../Services/shared-data.service';

describe('HomeComponent', () => {
    let fixture: ComponentFixture<HomeComponent>;
    let component: HomeComponent;
    let routerSpy: any;
    let toastrSpy: any;

    beforeEach(() => {

        TestBed.configureTestingModule({
            declarations: [
                SigninComponent,
                HomeComponent,
                ListPostsComponent,
                AddOrListCommentsComponent
            ],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: ToastrService, useValue: toastrSpy },
                AuthService,
                PostService,
                UserService,
                SharedDataService
            ],
            imports: [
                RouterTestingModule.withRoutes([{ path: 'signin', component: SigninComponent }]),
                FormsModule,
                ToastrModule,
                QuillModule,
                AvatarModule
            ]
        });

        fixture = TestBed.createComponent(HomeComponent);
        component = fixture.componentInstance;
        routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'info', 'error']);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /* it('should tell Router to navigate to signIn page when Signout clicked', () => {
        component.logout();

        // args passed to router.navigateByUrl() spy
        const spy = routerSpy.navigate as jasmine.Spy;
        const navArgs = spy.calls.first().args[0];

        expect(navArgs).toBe('/signin', 'should nav to signin page');
    }); */
});

