import { Post } from './Post';

export class User {
    constructor(
        public name: string,
        public email: string,
        public phone: number,
        public gender: string,
        public birthDate: Date,
        public password: string,
        public profileImage?: ByteString,
        public id?: number,
        public postId?: number[],
        public posts?: Post[]
    ) {}
}
