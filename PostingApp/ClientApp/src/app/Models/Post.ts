import {User} from './User';

export class Post {
    constructor(
        public content: string,
        public createdAt: Date,
        public userId: number,
        public likeData: number[],
        public id?: number,
        public user?: User,
        public comments?: Comment[],
        public commentCount?: number,
        public isPostLikedByLoggedInUser?: boolean
    ) {}
}
