import { User } from '../Models/User';

export class Comment {
    constructor(
        public content: string,
        public createdAt: Date,
        public userId: number,
        public postId: number,
        public id?: number,
        public user?: User
    ) {}
}
