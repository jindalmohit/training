import { Comment } from '../Models/Comment';
import { CommentService } from '../Services/comment.service';
import { defer } from 'rxjs';

let httpClientSpy: { get: jasmine.Spy };
let commentService: CommentService;

/** Create async observable that emits-once and completes
 *  after a JS engine turn */
export function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
}

/** Create async observable error that errors
*  after a JS engine turn */
export function asyncError<T>(errorObject: any) {
    return defer(() => Promise.reject(errorObject));
}

describe('CommentService', () => {
    beforeEach(() => {
        // TODO: spy on other methods too
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        commentService = new CommentService(<any>httpClientSpy);
    });

    it('should return expected comment on passing commentId (HttpClient called once)', () => {
        let expectedComment: Comment;
        expectedComment =  {
            'id': 1024,
            'content': 'So true!',
            'createdAt': new Date('2018-10-12T06:45:21.761'),
            'userId': 1,
            'postId': 20
        };

        httpClientSpy.get.and.returnValue(asyncData(expectedComment));

        commentService.getCommentById(1024).subscribe(
            comment => expect(comment).toEqual(expectedComment, expectedComment.id),
            fail
        );
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    });
});
