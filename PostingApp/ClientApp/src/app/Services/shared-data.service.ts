import { Injectable } from '@angular/core';
import { Post } from '../Models/Post';
import { PostService } from '../Services/post.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {

  constructor(private postService: PostService) { }

  public allPostsAsObservable = new BehaviorSubject<Post[]>([]);
  public allPostsAsObservable$ = this.allPostsAsObservable.asObservable();
}
