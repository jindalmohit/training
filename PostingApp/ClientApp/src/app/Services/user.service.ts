import { Injectable } from '@angular/core';
import { User } from '../Models/User';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiRoot = 'https://localhost:5001/api';

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<User[]> {
    console.log('entered user service');
    const apiURL = `${this.apiRoot}/users`;
    return this.http.get<User[]>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  getUserById(id: number): Observable<User> {
    const apiURL = `${this.apiRoot}/users/${id}`;
    return this.http.get<User>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  addUser(user: User): Observable<User> {
    const apiURL = `${this.apiRoot}/users`;
    return this.http.post<User>(apiURL, user)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  deleteUser(id: number): Observable<User> {
    const apiURL = `${this.apiRoot}/users/${id}`;
    return this.http.delete(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message);
  }
}
