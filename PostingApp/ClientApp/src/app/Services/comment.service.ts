import { Injectable } from '@angular/core';
import { Post } from '../Models/Post';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Comment } from '../Models/Comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  apiRoot = 'https://localhost:5001/api';

  constructor(private http: HttpClient) { }

  getAllComments(): Observable<Comment[]> {
    console.log('entered comment service');
    const apiURL = `${this.apiRoot}/comments`;
    return this.http.get<Comment[]>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  getCommentById(id: number): Observable<Comment> {
    const apiURL = `${this.apiRoot}/comments/${id}`;
    return this.http.get<Comment>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  getCommentsByPostId(postId: number): Observable<Comment[]> {
    const apiURL = `${this.apiRoot}/comments/postedComments/${postId}`;
    return this.http.get<Comment[]>(apiURL)
    .pipe(catchError((error, caught) => {
      console.log(error.message);
      return this.errorHandler(error);
    }));
  }

  addComment(comment: Comment): Observable<Comment> {
    console.log(comment);
    const apiURL = `${this.apiRoot}/comments`;
    return this.http.post<Comment>(apiURL, comment)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  editComment(comment: Comment): Observable<Comment> {
    const apiURL = `${this.apiRoot}/comments`;
    return this.http.put<Comment>(apiURL, comment)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  deleteComment(id: number): Observable<Comment> {
    const apiURL = `${this.apiRoot}/comments/${id}`;
    return this.http.delete<Comment>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message);
  }
}
