import { User } from '../Models/User';
import { UserService } from '../Services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { defer } from 'rxjs';

let httpClientSpy: { get: jasmine.Spy };
let userService: UserService;

/** Create async observable that emits-once and completes
 *  after a JS engine turn */
export function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
}

/** Create async observable error that errors
*  after a JS engine turn */
export function asyncError<T>(errorObject: any) {
    return defer(() => Promise.reject(errorObject));
}

describe('UserService', () => {
    beforeEach(() => {
        // TODO: spy on other methods too
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        userService = new UserService(<any>httpClientSpy);
    });

    it('should return expected users (HttpClient called once)', () => {
        let expectedUsers: User[];
        expectedUsers =
            [
                {
                    'id': 1,
                    'name': 'John Doe',
                    'email': 'john@gmail.com',
                    'phone': 1111122222,
                    'gender': 'Male',
                    'birthDate': new Date('2018-10-05T06:00:00'),
                    'profileImage': null,
                    'password': 'john123',
                    'posts': null
                },
                {
                    'id': 2,
                    'name': 'Richard Roe',
                    'email': 'roe@gmail.com',
                    'phone': 2222233333,
                    'gender': 'Male',
                    'birthDate': new Date('2018-10-07T18:00:00'),
                    'profileImage': null,
                    'password': 'roe123',
                    'posts': null
                },
                {
                    'id': 4,
                    'name': 'Chuck Jones',
                    'email': 'chuck@gmail.com',
                    'phone': 3333344444,
                    'gender': 'Male',
                    'birthDate': new Date('1999-10-07T00:00:00'),
                    'profileImage': null,
                    'password': 'chuck123',
                    'posts': null
                },
                {
                    'id': 5,
                    'name': 'Mohit Agarwal',
                    'email': 'mohit@gmail.com',
                    'phone': 1234567890,
                    'gender': 'Male',
                    'birthDate': new Date('2018-09-10T00:00:00'),
                    'profileImage': null,
                    'password': 'mohit123',
                    'posts': null
                }
            ];

        httpClientSpy.get.and.returnValue(asyncData(expectedUsers));

        userService.getAllUsers().subscribe(
            users => expect(users).toEqual(expectedUsers, 'expected users'),
            fail
        );
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    });

    it('should return expected user on passing userId (HttpClient called once)', () => {
        let expectedUser: User;
        expectedUser = {
            'id': 1,
            'name': 'John Doe',
            'email': 'john@gmail.com',
            'phone': 1111122222,
            'gender': 'Male',
            'birthDate': new Date('2018-10-05T06:00:00'),
            'profileImage': null,
            'password': 'john123',
            'posts': null
        };

        httpClientSpy.get.and.returnValue(asyncData(expectedUser));

        userService.getUserById(1).subscribe(
            user => expect(user).toEqual(expectedUser, expectedUser.name),
            fail
        );
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    });

    /* it('should return an error when the server returns a 404', () => {
        const errorResponse = new HttpErrorResponse({
            error: 'test 404 error',
            status: 404, statusText: 'Not Found'
        });

        httpClientSpy.get.and.returnValue(asyncError(errorResponse));

        userService.getAllUsers().subscribe(
            users => fail('expected an error, not users'),
            error => expect(error.message).toContain('404 Not Found')
        );
    });
     */
});

