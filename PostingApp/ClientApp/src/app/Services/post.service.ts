import { Injectable } from '@angular/core';
import { Post } from '../Models/Post';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  apiRoot = 'https://localhost:5001/api';

  constructor(private http: HttpClient) { }

  getAllPost(): Observable<Post[]> {
    console.log('entered post service');
    const apiURL = `${this.apiRoot}/posts`;
    return this.http.get<Post[]>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  getPostById(id: number): Observable<Post> {
    const apiURL = `${this.apiRoot}/posts/${id}`;
    return this.http.get<Post>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }) as any);
  }

  addPost(post: Post): Observable<Post> {
    const apiURL = `${this.apiRoot}/posts`;
    return this.http.post<Post>(apiURL, post)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }));
  }

  editPost(post: Post): Observable<Post> {
    console.log(post);
    const apiURL = `${this.apiRoot}/posts`;
    return this.http.put<Post>(apiURL, post)
    .pipe(catchError((error, caught) => {
      console.log(error.message);
      return this.errorHandler(error);
    }));
  }

  deletePost(id: number): Observable<Post> {
    const apiURL = `${this.apiRoot}/posts/${id}`;
    return this.http.delete<Post>(apiURL)
      .pipe(catchError((error, caught) => {
        console.log(error.message);
        return this.errorHandler(error);
      }));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message);
  }
}
