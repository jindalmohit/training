import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public loggedInStatus = false;

  constructor() { }

  setLoggedInStatus(status: boolean): void {
    this.loggedInStatus = status;
  }

  get isLoggedIn() {
    return this.loggedInStatus;
  }
}
