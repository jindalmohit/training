import { PostService } from '../Services/post.service';
import { UserService } from '../Services/user.service';
import { AuthService } from '../Services/auth.service';
import { Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { User } from '../Models/User';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';
import { AddOrListCommentsComponent } from '../add-or-list-comments/add-or-list-comments.component';
import { SharedDataService } from '../Services/shared-data.service';
import { CommentService } from '../Services/comment.service';

describe('AddOrListCommentsComponent', () => {
    let fixture: ComponentFixture<AddOrListCommentsComponent>;
    let component: AddOrListCommentsComponent;
    let routerSpy: any;
    let toastrSpy: any;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AddOrListCommentsComponent],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: ToastrService, useValue: toastrSpy },
                PostService,
                UserService,
                AuthService,
                CommentService,
                SharedDataService
            ],
            imports: [
                FormsModule,
                AvatarModule
            ]
        });

        fixture = TestBed.createComponent(AddOrListCommentsComponent);
        component = fixture.componentInstance;
        routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'info', 'error']);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('commentsOnSelectedPost should not be null after ngOnInit', () => {
        component.selectedPostToComment = {
            'id': 15,
            'content': '<p><strong>Hello<em> <u>Folks!</u></em></strong></p>',
            'createdAt': new Date('2018-10-08T13:18:07.026'),
            'likeData': [
                2
            ],
            'userId': 1
        };
        fixture.detectChanges();
        expect(component.commentsOnSelectedPost).not.toBeNull();
    });

    it('allPosts should not be null after ngOnInit', () => {
        component.selectedPostToComment = {
            'id': 15,
            'content': '<p><strong>Hello<em> <u>Folks!</u></em></strong></p>',
            'createdAt': new Date('2018-10-08T13:18:07.026'),
            'likeData': [
                2
            ],
            'userId': 1
        };
        fixture.detectChanges();
        expect(component.allPosts).not.toBeNull();
    });
});
