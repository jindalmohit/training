import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../Models/Comment';
import { Post } from '../Models/Post';
import { CommentService } from '../Services/comment.service';
import { User } from '../Models/User';
import { PostService } from '../Services/post.service';
import { UserService } from '../Services/user.service';
import { SharedDataService } from '../Services/shared-data.service';

@Component({
  selector: 'app-add-or-list-comments',
  templateUrl: './add-or-list-comments.component.html',
  styleUrls: ['./add-or-list-comments.component.css']
})
export class AddOrListCommentsComponent implements OnInit {

  public comment = new Comment('', null, null, null);
  public commentContent = '';
  @Input() public selectedPostToComment: Post;
  public commentsOnSelectedPost: Comment[] = [];
  public loggedInUser: User = localStorage.getItem('loggedInUser') == null ? null : JSON.parse(localStorage.getItem('loggedInUser'));
  @Output() public childEvent = new EventEmitter();
  public allPostsWithCommentCount: Post[];
  public commentsWithUserDetail: Comment[];
  public allPosts: Post[];

  constructor(
    private commentService: CommentService,
    private postService: PostService,
    private userService: UserService,
    private sharedDataService: SharedDataService
  ) { }

  ngOnInit() {
    if (this.selectedPostToComment) {
      console.log(this.selectedPostToComment);
    }
    this.commentService.getCommentsByPostId(this.selectedPostToComment.id).subscribe(
      res => {
        this.commentsOnSelectedPost = res;
        console.log(this.commentsOnSelectedPost);
        this.getCommentsWithUserDetail(this.commentsOnSelectedPost);
      }
    );
    this.postService.getAllPost().subscribe(
      res => {
        this.allPosts = res;
      }
    );
  }

  onKeyDown(event) {
    this.addComment(this.comment);
  }

  getCommentsWithUserDetail(comments: Comment[]) {
    this.commentsWithUserDetail = Object.assign([], comments);
    this.commentsWithUserDetail.forEach((element, index, arr) => {
      this.userService.getUserById(element.userId).subscribe(
        res => {
          element.user = res;
        }
      );
    });
  }

  addComment(comment: Comment): void {
    comment.content = this.commentContent;
    comment.userId = this.loggedInUser.id;
    comment.createdAt = new Date();
    comment.postId = this.selectedPostToComment.id;
    this.selectedPostToComment.commentCount++;

    this.commentService.addComment(comment).subscribe(
      res => {
        console.log(res);
        this.commentContent = '';
        /*  this.postService.editPost(this.selectedPostToComment).subscribe(
           r => {
             console.log(r);
           }
         ); */
        //  this.allPosts.forEach((element, index, arr) => {
        //    if (element.id === this.selectedPostToComment.id) {
        //      element.commentCount++;
        //      arr[index] = element;
        //    }
        //  });
        //  this.sharedDataService.allPostsAsObservable.next(this.allPosts);
       /*  this.postService.getAllPost().subscribe(
          rslt => {
            console.log(rslt);
            this.sharedDataService.allPostsAsObservable.next(rslt);
            // this.allPostsWithCommentCount = rslt;
            // this.childEvent.emit(this.allPostsWithCommentCount);
          }
        ); */
        this.commentService.getCommentsByPostId(this.selectedPostToComment.id).subscribe(
          result => {
            this.commentsOnSelectedPost = result;
            console.log(this.commentsOnSelectedPost);
            this.getCommentsWithUserDetail(this.commentsOnSelectedPost);
          }
        );
      }
    );
  }

}
