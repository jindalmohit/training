import { Component, OnInit } from '@angular/core';
import { User } from '../Models/User';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public allUsers: User[] = [];
  public genders = ['Male', 'Female'];

  public user = new User('', '', null, '', null, '');
  public confirmPass = '';
  private isPasswordMatched = false;
  private isNameInvalid = false;

  constructor(private userService: UserService, private router: Router, private toastrService: ToastrService) { }

  ngOnInit() {
    this.userService.getAllUsers().subscribe(
      res => this.allUsers = res,
      msg => console.error(`Error: ${msg.status} ${msg.statusText}`));
  }

  onChange(nameInputVal): void {
    if (nameInputVal.trim() === '' || nameInputVal.trim() === null) {
      this.isNameInvalid = true;
    } else {
      this.isNameInvalid = false;
    }
  }

  registerUser(): void {
    if (this.user.password !== '' && this.user.password === this.confirmPass) {
      this.isPasswordMatched = true;
      if (this.isUserAlreadyRegistered(this.user.email)) {
        this.toastrService.error('User with this email Id is already registered', 'Registration failed!');
      } else {
        this.userService.addUser(this.user).subscribe(
          res => {
            console.log(res);
            this.toastrService.success('Registered Successfully', 'Congratulations!');
            this.router.navigate(['/signin']);
          }
        );
      }
    }
  }

  isUserAlreadyRegistered(email: string): boolean {
    let count = 0;
    this.allUsers.forEach(eachUser => {
      if (eachUser.email === email) {
        count++;
      }
    });
    if (count === 0) {
      return false;
    } else {
      return true;
    }
  }
}
