import { SigninComponent } from '../signin/signin.component';
import { SignupComponent } from './signup.component';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AuthService } from '../Services/auth.service';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { User } from '../Models/User';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from '../home/home.component';
import { QuillModule } from 'ngx-quill';
import { ListPostsComponent } from '../list-posts/list-posts.component';
import { AvatarModule } from 'ngx-avatar';
import { AddOrListCommentsComponent } from '../add-or-list-comments/add-or-list-comments.component';
import { SharedDataService } from '../Services/shared-data.service';
import { By } from '@angular/platform-browser';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';

describe('Signup Component', () => {
    let fixture: ComponentFixture<SignupComponent>;
    let component: SignupComponent;
    // let linkDes: any;
    // let routerLinks: any;
    let userServiceStub: any;
    let getAllUsersSpy: any;
    // let routerSpy: any;
    let toastrSpy: any;

    beforeEach(() => {
        const routes: Routes = [
            { path: '', redirectTo: '/signin', pathMatch: 'full' },
            { path: 'signin', component: SigninComponent },
            { path: 'signup', component: SignupComponent },
            { path: 'home', component: HomeComponent},
            { path: '**', component: PageNotFoundComponent }
          ];

        let expectedUsers: User[];
        expectedUsers =
            [
                {
                    'id': 1,
                    'name': 'John Doe',
                    'email': 'john@gmail.com',
                    'phone': 1111122222,
                    'gender': 'Male',
                    'birthDate': new Date('2018-10-05T06:00:00'),
                    'profileImage': null,
                    'password': 'john123',
                    'posts': null
                },
                {
                    'id': 2,
                    'name': 'Richard Roe',
                    'email': 'roe@gmail.com',
                    'phone': 2222233333,
                    'gender': 'Male',
                    'birthDate': new Date('2018-10-07T18:00:00'),
                    'profileImage': null,
                    'password': 'roe123',
                    'posts': null
                },
                {
                    'id': 4,
                    'name': 'Chuck Jones',
                    'email': 'chuck@gmail.com',
                    'phone': 3333344444,
                    'gender': 'Male',
                    'birthDate': new Date('1999-10-07T00:00:00'),
                    'profileImage': null,
                    'password': 'chuck123',
                    'posts': null
                },
                {
                    'id': 5,
                    'name': 'Mohit Agarwal',
                    'email': 'mohit@gmail.com',
                    'phone': 1234567890,
                    'gender': 'Male',
                    'birthDate': new Date('2018-09-10T00:00:00'),
                    'profileImage': null,
                    'password': 'mohit123',
                    'posts': null
                }
            ];

        userServiceStub = jasmine.createSpyObj('UserService', ['getAllUsers']);
        getAllUsersSpy = userServiceStub.getAllUsers.and.returnValue(of(expectedUsers));
        // routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'info', 'error']);


        TestBed.configureTestingModule({
            declarations: [
                SignupComponent,
                SigninComponent,
                HomeComponent,
                ListPostsComponent,
                AddOrListCommentsComponent,
                PageNotFoundComponent
            ],
            providers: [
                { provide: UserService, useValue: userServiceStub },
                // { provide: Router, useValue: routerSpy },
                { provide: ToastrService, useValue: toastrSpy },
                AuthService,
                SharedDataService
            ],
            imports: [
                RouterTestingModule.withRoutes(routes),
                FormsModule,
                ToastrModule,
                QuillModule,
                AvatarModule
            ]
        });

        fixture = TestBed.createComponent(SignupComponent);
        component = fixture.componentInstance;
    });

    // beforeEach(() => {
    //     fixture.detectChanges(); // trigger initial data binding

    //     // find DebugElements with an attached RouterLinkStubDirective
    //     linkDes = fixture.debugElement
    //         .queryAll(By.directive(RouterLinkStubDirective));

    //     // get attached link directive instances
    //     // using each DebugElement's injector
    //     routerLinks = linkDes.map(de => de.injector.get(RouterLinkStubDirective));
    // });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain router-link element', () => {
        const debugElement = fixture.debugElement.query(By.css('.route'));
        expect(debugElement.attributes.routerLink).not.toBeNull();
    });

});
