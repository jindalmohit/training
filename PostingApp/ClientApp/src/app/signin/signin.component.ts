import { Component, OnInit } from '@angular/core';
import { User } from '../Models/User';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../Services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  public allUsers: User[] = [];
  public loggedInUser: User;
  userEmail: string;
  userPassword: string;
  public credentialsNotMatched = true;

  constructor(
    private userService: UserService,
    private router: Router,
    private toastrService: ToastrService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.userService.getAllUsers().subscribe(
      res => {
        console.log(res);
        this.allUsers = res;
        console.log(this.allUsers);
      },
      msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
    );
  }

  isAuthorised(): void {
    console.log('Username', this.userEmail);
    console.log('Password', this.userPassword);
    let count = 0;
    this.allUsers.forEach(eachUser => {
      if (eachUser.email === this.userEmail && eachUser.password === this.userPassword) {
        count++;
        this.loggedInUser = eachUser;
        console.log('LoggedIn User', this.loggedInUser);
      }
    });
    if (count !== 0) {
      this.credentialsNotMatched = false;
    }
    console.log(count);
  }

  login(): void {
    this.isAuthorised();
    if (this.credentialsNotMatched === true) {
      this.toastrService.error('Email-Id or password not matched', 'Unauthorised!');
    } else {
      console.log(this.credentialsNotMatched);
      localStorage.setItem('loggedInUser', JSON.stringify(this.loggedInUser));
      this.authService.setLoggedInStatus(true);
      this.router.navigate(['/home', { id: this.loggedInUser.id }]);

      /*  if (this.loggedInUser.role === 'Admin') {
         this.router.navigate(['/library/admin', { id: this.loggedInUser.id }]);
       } else {
         this.router.navigate(['/library/user', { id: this.loggedInUser.id }]);
       } */
      this.toastrService.success('You\'re Logged in', 'Authorised!', {
        positionClass: 'toast-bottom-right'
      });
      this.toastrService.info(`Welcome ${this.loggedInUser.name}`, '', {
        positionClass: 'toast-bottom-right'
      });
    }
  }

}
