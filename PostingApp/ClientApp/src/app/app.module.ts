import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { QuillModule } from 'ngx-quill';
import { ToastrModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AvatarModule } from 'ngx-avatar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { routingComponents } from './app-routing.module';
import { UserService } from './Services/user.service';
import { PostService } from './Services/post.service';
import { CommentService } from './Services/comment.service';
import { AuthService } from './Services/auth.service';
import { ListPostsComponent } from './list-posts/list-posts.component';
import { AddOrListCommentsComponent } from './add-or-list-comments/add-or-list-comments.component';
import { RouterLinkStubDirective } from './Mock-Directives/router-link-stub.directive';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ListPostsComponent,
    AddOrListCommentsComponent,
    RouterLinkStubDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    QuillModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    AvatarModule
  ],
  providers: [UserService, AuthService, PostService, CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
