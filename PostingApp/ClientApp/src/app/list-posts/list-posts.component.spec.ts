import { PostService } from '../Services/post.service';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { AuthService } from '../Services/auth.service';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { User } from '../Models/User';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { ListPostsComponent } from '../list-posts/list-posts.component';
import { AvatarModule } from 'ngx-avatar';
import { AddOrListCommentsComponent } from '../add-or-list-comments/add-or-list-comments.component';
import { SharedDataService } from '../Services/shared-data.service';
import { CommentService } from '../Services/comment.service';
import { Post } from '../Models/Post';


describe('ListPostsComponent', () => {
    let fixture: ComponentFixture<ListPostsComponent>;
    let component: ListPostsComponent;
    let routerSpy: any;
    let toastrSpy: any;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ListPostsComponent,
                AddOrListCommentsComponent
            ],
            providers: [
                { provide: Router, useValue: routerSpy },
                { provide: ToastrService, useValue: toastrSpy },
                AuthService,
                PostService,
                UserService,
                SharedDataService,
                CommentService
            ],
            imports: [
                RouterTestingModule.withRoutes([]),
                FormsModule,
                ToastrModule,
                QuillModule,
                AvatarModule
            ]
        });

        fixture = TestBed.createComponent(ListPostsComponent);
        component = fixture.componentInstance;
        routerSpy = jasmine.createSpyObj('Router', ['navigate']);
        toastrSpy = jasmine.createSpyObj('ToastrService', ['success', 'info', 'error']);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('allPosts should not be null after ngOnInit', () => {
        fixture.detectChanges();
        expect(component.allPosts).not.toBeNull();
    });

    it('allUsers should not be null after ngOnInit', () => {
        fixture.detectChanges();
        expect(component.allUsers).not.toBeNull();
    });

    it('allComments should not be null after ngOnInit', () => {
        fixture.detectChanges();
        expect(component.allComments).not.toBeNull();
    });

    it('isCommentBtnClicked should change after showOrHideComments()', () => {
        let post: Post;
        post = {
            'id': 15,
            'content': '<p><strong>Hello<em> <u>Folks!</u></em></strong></p>',
            'createdAt': new Date('2018-10-08T13:18:07.026'),
            'likeData': [
                2
            ],
            'userId': 1
        };
        component.isCommentBtnClicked = false;
        component.showOrHideComments(post);

        expect(component.isCommentBtnClicked).toEqual(true, component.isCommentBtnClicked);
    });

    it('selectedPostToComment should intialize after showOrHideComments()', () => {
        let post: Post;
        post = {
            'id': 15,
            'content': '<p><strong>Hello<em> <u>Folks!</u></em></strong></p>',
            'createdAt': new Date('2018-10-08T13:18:07.026'),
            'likeData': [
                2
            ],
            'userId': 1
        };
        component.isCommentBtnClicked = false;
        component.showOrHideComments(post);

        expect(component.selectedPostToComment).toEqual(post);
    });
});
