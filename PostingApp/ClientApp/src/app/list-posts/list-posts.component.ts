import { Component, OnInit, ElementRef, ViewChild, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import { PostService } from '../Services/post.service';
import { Post } from '../Models/Post';
import { User } from '../Models/User';
import { UserService } from '../Services/user.service';
import { CommentService } from '../Services/comment.service';
import { Comment } from '../Models/Comment';
import { SharedDataService } from '../Services/shared-data.service';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.css']
})
export class ListPostsComponent implements OnInit {

  public allPosts: Post[];
  @ViewChild('post') post: any;
  public allUsers: User[];
  public loggedInUser: User;
  public toggleComments: boolean[] = [];
  public selectedPostToComment: Post;
  public commentsOnSelectedPost: Comment[];
  public allComments: Comment[] = [];
  public allPostsWithCommentCount: Post[];
  @Output() childEvent = new EventEmitter();
  public isCommentBtnClicked = false;
  public allPostsWithUserDetails: Post[];
  public usersWhoLikedSelectedPost: User[];
  isModelDisplay = false;

  constructor(
    private postService: PostService,
    private userService: UserService,
    private commentService: CommentService,
    private sharedDataService: SharedDataService
  ) { }

  ngOnInit() {
    this.loggedInUser = localStorage.getItem('loggedInUser') === null ? null : JSON.parse(localStorage.getItem('loggedInUser'));
    this.postService.getAllPost().subscribe(
      res => {
        console.log(res);
        this.sharedDataService.allPostsAsObservable.next(res);
        /*   this.allPosts = res;
          console.log(this.allPosts);
          // this.getPostsWithComments(this.allPosts);
          this.getPostsWithUserDetails(this.allPosts); */
      },
      msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
    );

    this.sharedDataService.allPostsAsObservable.subscribe(
      result => {
        this.allPosts = result;
        this.getPostsWithUserDetails(this.allPosts);
      }
    );

    this.userService.getAllUsers().subscribe(
      res => {
        console.log(res);
        this.allUsers = res;
        console.log(this.allUsers);
      },
      msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
    );
    this.commentService.getAllComments().subscribe(
      res => {
        this.allComments = res;
        console.log(this.allComments);
      }
    );
  }

  getPostsWithUserDetails(posts) {
    this.allPostsWithUserDetails = Object.assign([], posts);
    this.allPostsWithUserDetails.forEach((element, index, arr) => {
      this.userService.getUserById(element.userId).subscribe(
        res => {
          element.user = res;
        }
      );
    });
    this.setIsPostLikedByLoggedInUser();
    console.log(this.allPostsWithUserDetails);
  }

  setIsPostLikedByLoggedInUser() {
    this.allPostsWithUserDetails.forEach((element, index, arr) => {
      if (element.likeData != null) {
        if (element.likeData.includes(this.loggedInUser.id)) {
          element.isPostLikedByLoggedInUser = true;
        } else {
          element.isPostLikedByLoggedInUser = false;
        }
      }
    });
  }

  // method(posts) {
  //   console.log(posts);
  //   this.allPostsWithCommentCount = posts;
  //   console.log(this.allPostsWithCommentCount);
  //   this.childEvent.emit(this.allPostsWithCommentCount);
  //   /* this.commentService.getAllComments().subscribe(
  //     res => {
  //       this.allComments = res;
  //       console.log(this.allComments);
  //     }
  //   ); */
  // }

  likePost(post: Post): void {
    if (post.isPostLikedByLoggedInUser === true) {
      post.isPostLikedByLoggedInUser = false;
      post.likeData = post.likeData.filter((element, index, arr) => {
        return (element !== this.loggedInUser.id);
      });
      console.log(post);
      this.postService.editPost(post).subscribe(
        res => {
          console.log(res);
          this.setIsPostLikedByLoggedInUser();
          console.log(this.allPosts);
        }
      );
    } else {
      if (post.likeData == null) {
        post.likeData = [];
      }
      post.likeData.push(this.loggedInUser.id);
      this.postService.editPost(post).subscribe(
        res => {
          console.log(res);
          this.setIsPostLikedByLoggedInUser();
          console.log(this.allPosts);
        }
      );
    }
  }

  showOrHideComments(post: Post): void {
    this.isCommentBtnClicked = this.isCommentBtnClicked === true ? false : true;
    if (this.toggleComments) {
      console.log(this.toggleComments[post.id]);
      this.toggleComments[post.id] = this.toggleComments[post.id] === true ? false : true;
      console.log(this.toggleComments[post.id]);
    }
    this.selectedPostToComment = post;
    console.log(this.selectedPostToComment);
  }

  showUsersWhoLikedThisPost(post: Post) {
    this.usersWhoLikedSelectedPost = [];
    post.likeData.forEach((element, index, arr) => {
      this.userService.getUserById(element).subscribe(
        res => {
          this.usersWhoLikedSelectedPost.push(res);
        }
      );
    });
    this.isModelDisplay = true;
    console.log(this.usersWhoLikedSelectedPost);
  }

  closeModal() {
    this.isModelDisplay = false;
  }
}

